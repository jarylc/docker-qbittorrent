FROM alpine

ARG QBT_VER
ARG LIB_VER

ENV PEER_PORT=6881 \
    WEB_PORT=8080 \
    UID=1000 \
    GID=1000

RUN set -e && \
    apk add --no-cache su-exec qt6-qtbase-sqlite && \
    apk add --no-cache --virtual .build-deps \
        boost-dev \
        boost-system \
        boost-thread \
        boost-build \
        ca-certificates \
        curl \
        g++ \
        make \
        cmake \
        qt6-qtbase \
        qt6-qttools-dev \
        tar && \
    mkdir -p /tmp/libtorrent-rasterbar && \
    cd /tmp/libtorrent-rasterbar/ && \
    curl -sSL ${LIB_VER} | tar xz --strip 1 && \
    export CORES=$(nproc) && \
    b2 install -j${CORES} --prefix=/usr/local --libdir=/usr/local/lib release \
    	toolset=gcc \
    	cxxstd=20 \
    	strip=on \
    	logging=off \
    	dht=on \
    	asserts=off  \
    	encryption=on \
    	crypto=openssl \
    	invariant-checks=off \
    	i2p=on \
    	profile-calls=off \
    	streaming=off \
    	super-seeding=off && \
    mkdir -p /tmp/qbittorrent && \
    cd /tmp/qbittorrent && \
    curl -sSL https://github.com/qbittorrent/qBittorrent/archive/${QBT_VER}.tar.gz | tar xz --strip 1 && \
    cmake -Wno-dev -B build \
        -D VERBOSE_CONFIGURE="ON" \
        -D CMAKE_INSTALL_PREFIX="/usr/local" \
        -D LibtorrentRasterbar_DIR="/usr/local/lib" \
        -D CMAKE_BUILD_TYPE="Release" \
        -D GUI="OFF" \
        -D DBUS="OFF" \
        -D STACKTRACE="OFF" && \
    cmake --build build -j ${CORES} && \
    cmake --install build && \
    cd / && \
    runDeps="$( \
        scanelf --needed --nobanner /usr/local/lib/libtorrent* /usr/local/bin/qbittorrent* \
            | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
            | xargs -r apk info --installed \
            | sort -u \
    )" && \
    apk add --no-cache --virtual .run-deps $runDeps && \
    apk del .build-deps && \
    rm -rf /tmp/*

COPY rootfs /

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["/usr/local/bin/qbittorrent-nox"]
