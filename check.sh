#!/bin/bash

apk add curl jq

[[ ! -f EXISTING_QBT ]] || touch EXISTING_QBT
EXISTING_QBT=$(cat EXISTING_QBT)
echo "Existing qBittorrent: ${EXISTING_QBT}"

if [[ -n $OVERWRITE_QBT ]]; then
  echo "Overwriting QBT: $OVERWRITE_QBT"
  LATEST_QBT=$OVERWRITE_QBT
else
  for i in {-1..-5}; do # check 5 versions, ignoring betas and rcs
    LATEST_QBT=$(curl -ks https://api.github.com/repos/qbittorrent/qBittorrent/git/refs/tags | jq -r ".[${i}].ref" | cut -d'/' -f3)
    if [[ $LATEST_QBT != *'beta'* && $LATEST_QBT != *'rc'* && $LATEST_QBT != *'alpha'* ]]; then
      echo "Latest qBittorrent: ${LATEST_QBT}"
      break
    else
      echo "Ignoring $LATEST_QBT"
    fi
  done
fi

[[ ! -f EXISTING_LIB ]] || touch EXISTING_LIB
EXISTING_LIB=$(cat EXISTING_LIB)

echo "Existing libtorrent: ${EXISTING_LIB}"
if [[ -n $OVERWRITE_LIB ]]; then
  echo "Overwriting LIB: $OVERWRITE_LIB"
  LATEST_LIB=$OVERWRITE_LIB
else
  for i in {0..4}; do # check 5 versions for a suitable libtorrent v2
    NAME=$(curl -ks https://api.github.com/repos/arvidn/libtorrent/releases | jq ".[${i}].name")
    if [[ $NAME == *'/2.'* || $NAME == *'-2.'* ]]; then
      ASSETS=$(curl -ks https://api.github.com/repos/arvidn/libtorrent/releases | jq -r ".[${i}].assets")
      for (( k=0; k<$(echo "${ASSETS}" | jq length); k++ )); do
        LATEST_LIB=$(echo "${ASSETS}" | jq -r ".[${k}].browser_download_url")
        if [[ ${LATEST_LIB} == *'libtorrent'* && ${LATEST_LIB} == *'.tar'* ]]; then
          echo "Latest libtorrent: ${LATEST_LIB}"
          break
        fi
      done
      break
    else
      echo "Ignoring $NAME"
    fi
  done
fi

if [[ (-n "${LATEST_QBT}" && "${LATEST_QBT}" != "${EXISTING_QBT}") || (-n "${LATEST_LIB}" && "${LATEST_LIB}" != "${EXISTING_LIB}") ]]; then
  mv build.template.yml build.yml
  sed -i "s \$LATEST_QBT ${LATEST_QBT} g" 'build.yml'
  sed -i "s \$LATEST_LIB ${LATEST_LIB} g" 'build.yml'

  echo "Building..."
fi
